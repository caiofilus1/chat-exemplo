const Sequelize = require("sequelize");

module.exports = (sequelize) => {
    let Messages = sequelize.define('Messages', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        text: {
            type: Sequelize.TEXT,
        },
        name: {
            type: Sequelize.TEXT,
        }
    });
    return Messages;
};