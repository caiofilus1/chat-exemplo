const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");
const userModel = require('./user.model');
const messageModel = require('./message.model');

let sequelize = new Sequelize('vueDatabase', 'vueUser', 'vueP4ssw@rd', {host: "helloiot.com.br", dialect: "mysql"});
sequelize.authenticate().then(() => {
    console.log("banco conetado");
}).catch(() => {
    console.log("erro ao conectar banco");
});

const Users = userModel(sequelize);
const Messages = messageModel(sequelize);

let Groups = sequelize.define('Groups', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: Sequelize.STRING,
    },
});

Users.belongsToMany(Groups, {through: 'UsersGroups', foreignKey: 'UserId'});
Groups.belongsToMany(Users, {through: 'UsersGroups', foreignKey: 'GroupId'});
Groups.hasMany(Messages);
Messages.belongsTo(Groups);

// sequelize.sync({alter: true});
// sequelize.sync();

sequelize.Groups = Groups;
sequelize.Users = Users;
sequelize.Messages = Messages;


module.exports = sequelize;