const express = require('express');
const router = express.Router();
const {Users,} = require('../models');

const jwt = require('jsonwebtoken');


router.post('/login', async (req, res) => {
    let {email, password} = req.body;
    try {
        let user = await Users.findOne({where: {email, password}});
        if (!user) return res.status(401).json({msg: 'Usuário não Encontrado'});
        let token = jwt.sign({id: user.id}, "Chave Secreta", {
            expiresIn: 300 // expires in 5min
        });
        res.set('token', token);
        user.password = undefined;
        return res.json(user);
    } catch (e) {
        console.log(e);
        return res.status(500).json({msg: 'Erro no banco De Dados'});
    }
});


router.post('/register', async (req, res) => {
    try {
        let emailUsers = await Users.findOne({where: {email: req.body.email}});
        if (emailUsers) return res.status(500).json({msg: 'Email já Utilizado'});
        let user = await Users.create(req.body);
        let token = jwt.sign({id: user.id}, "Chave Secreta", {
            expiresIn: 300 // expires in 5min
        });
        res.set('token', token);
        user.password = undefined;
        return res.json(user);
    } catch (e) {
        console.log(e);
        return res.status(500).json({msg: 'Erro ao Criar Usuário'});
    }
});

module.exports = router;