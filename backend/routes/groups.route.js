const express = require('express');
const router = express.Router();
const {Groups, Users} = require('../models');
const verifyJWT = require('../middleware/auth.js');

router.use(verifyJWT);

router.get('/', (req, res) => {
    Groups.findAll({
        where: {'$Users.id$': req.user.id},
        include: {
            model: Users,
        }
    }).then(result => {
        result = result || [];
        res.json(result);
    }).catch(err => {
        console.log(err);
        res.status(500).json({msg: err});
    });
});

router.post('/', (req, res) => {
    // let users = await User.findAll({where: {id: req.body.users}});
    Groups.create(req.body).then(async result => {
        console.log(result.setUsers);
        result.setUsers(req.body.users);
        res.json({id: result.id});
    }).catch(err => {
        console.log(err);
        res.status(500).json({msg: err});
    });
});

router.put('/:id', (req, res) => {
    Groups.update(req.body, {where: {id: req.params.id}}).then(result => {
        res.sendStatus(200);
    }).catch(err => {
        res.status(500).json({msg: err});
    });
});

router.delete('/:id', (req, res) => {
    Groups.destroy({where: {id: req.params.id}}).then(result => {
        res.sendStatus(200);
    }).catch(err => {
        res.status(500).json({msg: err});
    });
});

module.exports = router;