const express = require('express');
const router = express.Router();
const {Users,} = require('../models');
const verifyJWT = require('../middleware/auth.js');


router.use(verifyJWT);

router.get('/', (req, res) => {
    Users.findAll({attributes: {exclude: ["password"]}}).then(result => {
        result = result || [];
        res.json(result);
    }).catch(err => {
        res.status(500).json({msg: err});
    });
});

router.post('/', (req, res) => {
    Users.create(req.body).then(result => {
        res.json({id: result.id});
    }).catch(err => {
        res.status(500).json({msg: err});
    });
});

router.put('/:id', (req, res) => {
    Users.update(req.body, {where: {id: req.params.id}}).then(result => {
        res.sendStatus(200);
    }).catch(err => {
        res.status(500).json({msg: err});
    });
});

router.delete('/:id', (req, res) => {
    Users.destroy({where: {id: req.params.id}}).then(result => {
        res.sendStatus(200);
    }).catch(err => {
        res.status(500).json({msg: err});
    });
});

module.exports = router;