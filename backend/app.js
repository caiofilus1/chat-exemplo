const express = require('express');
const cors = require('cors');
const AuthRoute = require('./routes/auth.router');
const UserRoute = require('./routes/user.route');
const GroupsRoute = require('./routes/groups.route');
const app = express();
const ioServer = require('http').createServer(express());
const io = require('socket.io')(ioServer);

const {Messages} = require('./models');

app.use(express.json());
app.use(cors({
    exposedHeaders: 'token',
}));
app.use('/', AuthRoute);
app.use('/users', UserRoute);
app.use('/groups', GroupsRoute);

//
// const mqtt = require('mqtt');
// let client = mqtt.connect('mqtt://m24.cloudmqtt.com:13285', {
//     username: 'fntixbuk',
//     password: 'rDgOrbVvHLsB',
//     protocolId: 'MQIsdp',
//     protocolVersion: 3,
// });
//
// client.on('connect', function () {
//     client.subscribe('group/+');
// });
// client.on('error', function (e) {
//     console.log(e);
// });
//
// client.on('message', function (topic, message) {
//     let payload = JSON.parse(message.toString());
//     console.log(payload);
//     payload.GroupId = payload.groupId;
//     Messages.create(payload);
// });
//

io.on('connect', function (socket) {
    console.log("socket connectado");
    socket.on('joinGroup', (data) => {
        console.log(data);
        data = typeof data === "string" ? JSON.parse(data) : data;
        let groupId = data.groupId;
        socket.join('group/' + groupId);
    });
    socket.on('message', (data) => {
        data = typeof data === "string" ? JSON.parse(data) : data;
        let groupId = data.groupId;
        console.log(socket.adapter.rooms);
        Messages.create(data);
        io.to('group/' + groupId).emit('message', data);
    });
    socket.on('leaveGroup', (data) => {
        data = typeof data === "string" ? JSON.parse(data) : data;
        let groupId = data.groupId;
        socket.leave('group/' + groupId);
    });
});


ioServer.listen(4000, function () {
    console.log('Servidor rodando em: http://localhost:4000');
});

module.exports = app;
