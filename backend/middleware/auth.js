const jwt = require('jsonwebtoken');
const {Users,} = require('../models');

function verifyJWT(req, res, next) {
    var token = req.headers['token'];
    if (!token) return res.status(401).json({auth: false, mgs: 'Sem token'});

    jwt.verify(token, "Chave Secreta", async function (err, decoded) {
        if (err) return res.status(500).json({auth: false, msg: 'Falha ao authenticar'});
        try {
            let user = await Users.findOne({where: {id: decoded.id}});
            if (!user) return res.status(500).json({auth: false, msg: 'Usuário não Encontrado'});
            req.user = user;
            next();
        } catch (e) {
            return res.status(500).json({auth: false, msg: 'Erro no banco de Dados'});
        }
    });
}

module.exports = verifyJWT;