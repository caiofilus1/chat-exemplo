import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import VueSocketIO from 'vue-socket.io'

// import VueMqtt from 'vue-mqtt';

const axios = require('axios');

let user = require('./Utils/user');

axios.defaults.baseURL = "http://localhost:3000";

axios.interceptors.request.use(function (config) {
    config.headers.token = user.token;
    return config;
}, function (error) {
    return Promise.reject(error);
});
axios.interceptors.response.use(function (response) {
    // Do something with response data
    if (response.headers.token) {
        user.token = response.headers.token;
    }
    return response;
}, function (error) {
    // Do something with response error
    return Promise.reject(error);
});

Vue.config.productionTip = false;

// Vue.use(VueMqtt, 'wss://m24.cloudmqtt.com:33285/mqtt', {
//     username: 'fntixbuk',
//     password: 'rDgOrbVvHLsB',
//     protocolId: 'MQIsdp',
//     protocolVersion: 3,
// });

Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://localhost:4000' //options object is Optional
  })
);


// let client = mqtt.connect('mqtts://m24.cloudmqtt.com:13285', {
//     useSSL: true,
//     rejectUnauthorized: false,
//     username: 'fntixbuk',
//     password: 'rDgOrbVvHLsB',
// });
// client.on('connect', function () {
//     client.subscribe('presence', function (err) {
//         if (!err) {
//             client.publish('presence', 'Hello mqtt')
//         }
//     })
// });

// Vue.use(VueMqtt, 'ws://broker.hivemq.com:8000/mqtt', {
//     useSSL: true,
//     // username: 'fntixbuk',
//     // password: 'rDgOrbVvHLsB',
// });

Vue.prototype.$http = axios;
Vue.prototype.$user = user;


new Vue({
    router,
    vuetify,
    render: h => h(App)
}).$mount('#app');
