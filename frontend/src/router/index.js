import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '../views/Main.vue'
import Login from "../views/Login";
import Group from "../views/Group";

const user = require('../Utils/user');


Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Main',
        component: Main,
        children: [
            {
                path: 'group/:name',
                name: 'Group',
                component: Group,
                props: true,
            }
        ],


    }, {
        path: '/login',
        name: 'Login',
        component: Login
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = !!user.token;
    console.log(authRequired);
    console.log(loggedIn);
    if (authRequired && !loggedIn) {
        return next('/login');
    }
    return next();
});

export default router
